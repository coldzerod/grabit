package com.grabit.grabit.ui.activities;

import java.util.ArrayList;

import org.json.JSONException;

import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.MenuItem.OnMenuItemClickListener;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.grabit.grabit.R;
import com.grabit.grabit.model.AppRater;
import com.grabit.grabit.services.CommunicateService;
import com.grabit.grabit.utils.UserData;

public class MainActivity extends SherlockActivity {

	private AdView adView;
	private MultiAutoCompleteTextView actv;
	private UserData userData = UserData.getInstance();
	private boolean doubleBackToExitPressedOnce = false;
	private CommunicateService imService;
	private Boolean flag = false;

	ImageView imgView;

	String value = null;

	private ServiceConnection mConnection = new ServiceConnection() {
		public void onServiceConnected(ComponentName className, IBinder service) {
			// This is called when the connection with the service has been
			// established, giving us the service object we can use to
			// interact with the service. Because we have bound to a explicit
			// service that we know is running in our own process, we can
			// cast its IBinder to a concrete class and directly access it.
			imService = ((CommunicateService.IMBinder) service).getService();
			flag = true;
			//
			if (imService.isUserAuthenticated() == true) {
				Intent i = new Intent(MainActivity.this,
						grabitGalleryActivity.class);
				startActivity(i);
				MainActivity.this.finish();
			}
		}

		public void onServiceDisconnected(ComponentName className) {
			// This is called when the connection with the service has been
			// unexpectedly disconnected -- that is, its process crashed.
			// Because it is running in our same process, we should never
			// see this happen.
			imService = null;
			Toast.makeText(MainActivity.this, "The service has disconnected",
					Toast.LENGTH_SHORT).show();
		}
	};

	private boolean isNetworkAvailable() {
		boolean available = false;
		/** Getting the system's connectivity service */
		ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

		/** Getting active network interface to get the network's status */
		NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

		if (networkInfo != null && networkInfo.isAvailable())
			available = true;

		/** Returning the status of the network */
		return available;
	}

	@Override
	public void onBackPressed() {
		if (doubleBackToExitPressedOnce) {
			super.onBackPressed();
			return;
		}

		this.doubleBackToExitPressedOnce = true;
		Toast.makeText(this, "Please click BACK again to exit",
				Toast.LENGTH_SHORT).show();

		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				doubleBackToExitPressedOnce = false;
			}
		}, 2000);
	}

	public void onClick(View view) {

		{
			/** Getting a reference to Edit text containing url */
			EditText etUrl = (EditText) findViewById(R.id.et_url);

			getWindow().setSoftInputMode(
					WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

			findViewById(R.id.btn_profile_covers).setVisibility(View.GONE);
			findViewById(R.id.btn_profile_pictures).setVisibility(View.GONE);

			/** Starting the task created above */

			DownloadTask downloadTask = new DownloadTask();
			downloadTask.execute(etUrl.getText().toString());

		}
	}

	public void onClickProfile(View view) {

		Intent galleryIntent = new Intent(getBaseContext(),
				grabitGalleryActivity.class);
		galleryIntent.putExtra("type", true);
		startActivity(galleryIntent);

	}

	public void onClickCover(View view) {

		Intent galleryIntent = new Intent(getBaseContext(),
				grabitGalleryActivity.class);
		galleryIntent.putExtra("type", false);
		startActivity(galleryIntent);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		startService(new Intent(MainActivity.this, CommunicateService.class));

		// Get a Tracker (should auto-report)
		Tracker t=((MyApplication) getApplication())
				.getTracker(MyApplication.TrackerName.APP_TRACKER);

		t.enableAdvertisingIdCollection(true);
		
		enableAdMob();

		userData.init(this);

		LoadTask loadtask = new LoadTask();
		loadtask.execute();

	}

	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		// First Menu Button
		menu.add("Help")
				.setOnMenuItemClickListener(this.HelpButtonClickListener)
				.setIcon(R.drawable.question) // Set the menu icon
				.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

		// Third Menu Button
		menu.add("About")
				.setOnMenuItemClickListener(this.AboutButtonClickListener)
				.setIcon(R.drawable.info) // Set the menu icon
				.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

		return super.onCreateOptionsMenu(menu);
	}

	// Capture first menu button click
	OnMenuItemClickListener HelpButtonClickListener = new OnMenuItemClickListener() {

		public boolean onMenuItemClick(MenuItem item) {

			Intent intent = new Intent(getApplicationContext(),
					HelpActivity.class);
			startActivity(intent);

			return false;
		}
	};

	// Capture third menu button click
	OnMenuItemClickListener AboutButtonClickListener = new OnMenuItemClickListener() {

		public boolean onMenuItemClick(MenuItem item) {

			imService.About();

			return false;
		}
	};

	private class LoadTask extends AsyncTask<Void, Void, String[]> {

		ProgressDialog pdLoading = new ProgressDialog(MainActivity.this);

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			// this method will be running on UI thread
			pdLoading.setMessage("\t Loading...");
			pdLoading.show();
		}

		@Override
		protected String[] doInBackground(Void... param) {

			ArrayList<String> history = userData.getUserHistory();

			String[] result = new String[history.size()];
			result = history.toArray(result);
			return result;
		}

		@Override
		protected void onPostExecute(String[] result) {

			@SuppressWarnings({ "rawtypes", "unchecked" })
			ArrayAdapter adapter = new ArrayAdapter(MainActivity.this,
					android.R.layout.simple_list_item_1, result);

			actv = (MultiAutoCompleteTextView) findViewById(R.id.et_url);

			actv.setAdapter(adapter);

			actv.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());

			actv.setThreshold(1);

			pdLoading.dismiss();
		}

	}

	private class DownloadTask extends
			AsyncTask<String, Integer, ArrayList<String>> {

		ProgressDialog pdLoading = new ProgressDialog(MainActivity.this);

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			// this method will be running on UI thread
			pdLoading.setMessage("\t Please Wait...");
			pdLoading.show();
		}

		@Override
		protected ArrayList<String> doInBackground(String... url) {

			if (isNetworkAvailable()) {

				while (!flag) {
					if (flag)
						break;
				}

				imService.LoadProfilesPictures(url[0]);

				try {
					imService.LoadProfilesCover(url[0]);
				} catch (JSONException e) {
					e.printStackTrace();
				}

				while (!imService.isDataLoaded) {
					if (imService.isDataLoaded)
						break;
				}

				return imService.downloaded_profiles_names;
			} else {
				return null;
			}

		}

		@Override
		protected void onPostExecute(ArrayList<String> result) {
			/**
			 * Getting a reference to ImageView to display the downloaded image
			 */

			String nonCorrectProfiles = "";
			Boolean foundNonCorrect = false;
			Integer numberOfCorrect = 0;
			if (result != null) {

				pdLoading.dismiss();

				if (result.contains(null))
					foundNonCorrect = true;

				numberOfCorrect = imService.downloaded_profile_pictures.size();
				if (numberOfCorrect == 0) {
					Toast.makeText(
							getBaseContext(),
							"All Profile names provided are wrong!"
									+ nonCorrectProfiles, Toast.LENGTH_SHORT)
							.show();
					return;
				} else if (foundNonCorrect)
					Toast.makeText(getBaseContext(),
							"Some Profile names provided are Wrong ",
							Toast.LENGTH_SHORT).show();


				if (imService.downloaded_cover_pictures.size() > 0) {
					findViewById(R.id.btn_profile_covers).setVisibility(
							View.VISIBLE);
				}

				if (imService.downloaded_profile_pictures.size() > 0) {
					findViewById(R.id.btn_profile_pictures).setVisibility(
							View.VISIBLE);
				}

			} else {
				pdLoading.dismiss();

				Toast.makeText(getBaseContext(), "Network is not Available",
						Toast.LENGTH_SHORT).show();

			}

		}
	}

	protected void enableAdMob() {

		adView = (AdView) findViewById(R.id.adView);
		adView.setVisibility(View.GONE);

		adView.setAdListener(new AdListener() {
			@Override
			public void onAdLoaded() {
				super.onAdLoaded();
				adView.setVisibility(View.VISIBLE);
			}
		});
		AdRequest.Builder ads = new AdRequest.Builder();
		ads.addTestDevice("03B293FAF6DFB2307FF7550EA6CA173B");
		AdRequest adRequest = ads.build();
		adView.loadAd(adRequest);
	}

	@Override
	protected void onPause() {
		unbindService(mConnection);
		if (adView != null) {
			adView.pause();
		}
		super.onPause();
	}

	@Override
	protected void onRestart() {
		super.onRestart();
	}

	@Override
	protected void onResume() {

		bindService(new Intent(MainActivity.this, CommunicateService.class),
				mConnection, Context.BIND_AUTO_CREATE);

		if (adView != null) {
			adView.resume();
		}

		LoadTask loadtask = new LoadTask();
		loadtask.execute();

		super.onResume();
	}

	@Override
	protected void onDestroy() {
		if (adView != null) {
			adView.destroy();
		}
		super.onDestroy();
	}

	@Override
	public void onStart() {
		super.onStart();
		GoogleAnalytics.getInstance(this).reportActivityStart(this);
		AppRater.app_launched(this);
	}

	@Override
	public void onStop() {
		super.onStop();
		GoogleAnalytics.getInstance(this).reportActivityStop(this);
	}

}
