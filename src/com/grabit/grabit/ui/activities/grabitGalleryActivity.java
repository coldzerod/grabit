package com.grabit.grabit.ui.activities;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.MenuItem.OnMenuItemClickListener;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.grabit.grabit.R;
import com.grabit.grabit.services.CommunicateService;
import com.grabit.grabit.ui.adapters.ViewPagerAdapter;
import com.grabit.grabit.utils.UserData;

public class grabitGalleryActivity extends SherlockActivity {

	private ArrayList<Bitmap> imageArra = new ArrayList<Bitmap>();
	private AdView adView;
	private ViewPager myPager;
	private CommunicateService imService;
	private Boolean flag = false;
	private UserData userData = UserData.getInstance();
	private Boolean type_of_gallery;
	private ServiceConnection mConnection = new ServiceConnection() {
		public void onServiceConnected(ComponentName className, IBinder service) {
			// This is called when the connection with the service has been
			// established, giving us the service object we can use to
			// interact with the service. Because we have bound to a explicit
			// service that we know is running in our own process, we can
			// cast its IBinder to a concrete class and directly access it.

			imService = ((CommunicateService.IMBinder) service).getService();
			flag = true;
			//
			if (imService.isUserAuthenticated() == true) {
				Intent i = new Intent(grabitGalleryActivity.this,
						grabitGalleryActivity.class);
				startActivity(i);
				grabitGalleryActivity.this.finish();
			}
		}

		public void onServiceDisconnected(ComponentName className) {
			// This is called when the connection with the service has been
			// unexpectedly disconnected -- that is, its process crashed.
			// Because it is running in our same process, we should never
			// see this happen.
			imService = null;
			Toast.makeText(grabitGalleryActivity.this,
					"The service has disconnected", Toast.LENGTH_SHORT).show();
		}
	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.grapit_gallery_activity);

		type_of_gallery = getIntent().getExtras().getBoolean("type");
		// imageArra = userData.getDownloadedPicture();
		// userData.getDownloadedPicture();

		enableAdMob();

		LoadTask loadtask = new LoadTask();
		loadtask.execute(type_of_gallery);
	}

	protected void enableAdMob() {

		adView = (AdView) findViewById(R.id.adView);
		adView.setVisibility(View.GONE);

		adView.setAdListener(new AdListener() {
			@Override
			public void onAdLoaded() {
				super.onAdLoaded();
				adView.setVisibility(View.VISIBLE);
			}
		});
		AdRequest.Builder ads = new AdRequest.Builder();
		ads.addTestDevice("03B293FAF6DFB2307FF7550EA6CA173B");
		AdRequest adRequest = ads.build();
		adView.loadAd(adRequest);
	}

	public Boolean saveImage(int pos) {

		if (imageArra.get(pos) != null) {
			Bitmap bitmap = imageArra.get(pos);
			insertImage(getContentResolver(), bitmap, "grapit", "grapit");
			return true;

		} else {
			return false;

		}
	}

	public static final String insertImage(ContentResolver cr, Bitmap source,
			String title, String description) {

		ContentValues values = new ContentValues();
		values.put(Images.Media.TITLE, title);
		values.put(Images.Media.DISPLAY_NAME, title);
		values.put(Images.Media.DESCRIPTION, description);
		values.put(Images.Media.MIME_TYPE, "image/jpeg");
		// Add the date meta data to ensure the image is added at the front of
		// the gallery
		values.put(Images.Media.DATE_ADDED, System.currentTimeMillis());
		values.put(Images.Media.DATE_TAKEN, System.currentTimeMillis());

		Uri url = null;
		String stringUrl = null; /* value to be returned */

		try {
			url = cr.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
					values);

			if (source != null) {
				OutputStream imageOut = cr.openOutputStream(url);
				try {
					source.compress(Bitmap.CompressFormat.JPEG, 100, imageOut);
				} finally {
					imageOut.close();
				}

				long id = ContentUris.parseId(url);
				// Wait until MINI_KIND thumbnail is generated.
				Bitmap miniThumb = Images.Thumbnails.getThumbnail(cr, id,
						Images.Thumbnails.MINI_KIND, null);
				// This is for backward compatibility.
				storeThumbnail(cr, miniThumb, id, 50F, 50F,
						Images.Thumbnails.MICRO_KIND);
			} else {
				cr.delete(url, null, null);
				url = null;
			}
		} catch (Exception e) {
			if (url != null) {
				cr.delete(url, null, null);
				url = null;
			}
		}

		if (url != null) {
			stringUrl = url.toString();
		}

		return stringUrl;
	}

	private static final Bitmap storeThumbnail(ContentResolver cr,
			Bitmap source, long id, float width, float height, int kind) {

		// create the matrix to scale it
		Matrix matrix = new Matrix();

		float scaleX = width / source.getWidth();
		float scaleY = height / source.getHeight();

		matrix.setScale(scaleX, scaleY);

		Bitmap thumb = Bitmap.createBitmap(source, 0, 0, source.getWidth(),
				source.getHeight(), matrix, true);

		ContentValues values = new ContentValues(4);
		values.put(Images.Thumbnails.KIND, kind);
		values.put(Images.Thumbnails.IMAGE_ID, (int) id);
		values.put(Images.Thumbnails.HEIGHT, thumb.getHeight());
		values.put(Images.Thumbnails.WIDTH, thumb.getWidth());

		Uri url = cr.insert(Images.Thumbnails.EXTERNAL_CONTENT_URI, values);

		try {
			OutputStream thumbOut = cr.openOutputStream(url);
			thumb.compress(Bitmap.CompressFormat.JPEG, 100, thumbOut);
			thumbOut.close();
			return thumb;
		} catch (FileNotFoundException ex) {
			return null;
		} catch (IOException ex) {
			return null;
		}
	}

	// Capture first menu button click
	OnMenuItemClickListener SaveButtonClickListener = new OnMenuItemClickListener() {

		public boolean onMenuItemClick(MenuItem item) {

			DownloadTask downloadtask = new DownloadTask();
			downloadtask.execute(myPager.getCurrentItem());
			return false;
		}
	};

	// Capture second menu button click
	OnMenuItemClickListener SaveAllButtonClickListener = new OnMenuItemClickListener() {

		public boolean onMenuItemClick(MenuItem item) {

			DownloadTask downloadtask = new DownloadTask();
			downloadtask.execute(-1);
			return false;
		}
	};

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// First Menu Button
		menu.add("Save")
				.setOnMenuItemClickListener(this.SaveButtonClickListener)
				.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

		// Second Menu Button
		menu.add("Save All")
				.setOnMenuItemClickListener(this.SaveAllButtonClickListener)
				.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

		return super.onCreateOptionsMenu(menu);
	}

	private class LoadTask extends AsyncTask<Boolean, Void, Void> {

		ProgressDialog pdLoading = new ProgressDialog(
				grabitGalleryActivity.this);

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			// this method will be running on UI thread
			pdLoading.setMessage("\t Loading Gallery...");
			pdLoading.show();
		}

		@Override
		protected Void doInBackground(Boolean... params) {

			while (!flag) {
				if (flag)
					break;
			}
			if (params[0])
				imageArra = imService.downloaded_profile_pictures;
			else
				imageArra = imService.downloaded_cover_pictures;

			return null;

		}

		@Override
		protected void onPostExecute(Void result) {
			ViewPagerAdapter adapter = new ViewPagerAdapter(
					grabitGalleryActivity.this, imageArra);

			myPager = (ViewPager) findViewById(R.id.myfivepanelpager);

			myPager.setAdapter(adapter);

			myPager.setCurrentItem(0);

			pdLoading.dismiss();

			String temp;
			for (int i = 0; i < imService.downloaded_profiles_names.size(); i++) {
				temp = imService.downloaded_profiles_names.get(i);
				if (temp != null)
					userData.addUserHistory(temp);
			}
		}
	}

	private class DownloadTask extends AsyncTask<Integer, Boolean, Boolean> {
		ProgressDialog pdLoading = new ProgressDialog(
				grabitGalleryActivity.this);

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			// this method will be running on UI thread
			pdLoading.setMessage("\t Please Wait...");
			pdLoading.show();
		}

		@Override
		protected Boolean doInBackground(Integer... option) {

			Boolean res;
			if (option[0] == -1) {

				for (int i = 0; i < imageArra.size(); i++) {
					res = saveImage(i);
				}
				res = true;
			} else {
				res = saveImage(option[0]);
			}

			return res;
		}

		@Override
		protected void onPostExecute(Boolean result) {

			pdLoading.dismiss();

			if (!result)
				Toast.makeText(getApplicationContext(), "No Image To Save!",
						Toast.LENGTH_SHORT).show();
			else
				Toast.makeText(getApplicationContext(), "Image Saved",
						Toast.LENGTH_SHORT).show();

		}
	}

	@Override
	protected void onPause() {
		unbindService(mConnection);
		if (adView != null) {
			adView.pause();
		}
		super.onPause();
	}

	@Override
	protected void onRestart() {
		super.onRestart();
	}

	@Override
	protected void onResume() {
		bindService(new Intent(grabitGalleryActivity.this,
				CommunicateService.class), mConnection,
				Context.BIND_AUTO_CREATE);
		if (adView != null) {
			adView.resume();
		}

		super.onResume();
	}

	@Override
	protected void onDestroy() {
		if (adView != null) {
			adView.destroy();
		}
		super.onDestroy();
	}

	@Override
	public void onStart() {
		super.onStart();
		GoogleAnalytics.getInstance(this).reportActivityStart(this);
	}

	@Override
	public void onStop() {
		super.onStop();
		GoogleAnalytics.getInstance(this).reportActivityStop(this);
	}
}