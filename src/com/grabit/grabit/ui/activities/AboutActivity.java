package com.grabit.grabit.ui.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockActivity;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.grabit.grabit.R;

public class AboutActivity extends SherlockActivity {

	String type;
	LinearLayout layoutOfPopup;
	PopupWindow popupMessage;
	Button insidePopupButton;
	TextView popupText;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		type = getIntent().getExtras().getString("type");

		if (type.indexOf("email") != -1) {
			througMail();
		} else if (type.indexOf("about") != -1) {
			show_about();
		} else if (type.indexOf("self") != -1) {
			setContentView(R.layout.about_activity);
		} else
			finish();
	}

	private void show_about() {

		Intent intent = new Intent(this.getApplicationContext(),
				AboutActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
		intent.putExtra("type", "self");
		startActivity(intent);
		finish();
		//Log.v("starting activity back", "started");

	}

	private void througMail() {
		Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
				"mailto", "ConnectMe@gmail.com", null));
		emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Application Feedback");
		startActivity(Intent.createChooser(emailIntent, "Send FeedBack"));
		finish();
	}
	
	@Override
	public void onStart() {
		super.onStart();
		GoogleAnalytics.getInstance(this).reportActivityStart(this);
	}

	@Override
	public void onStop() {
		super.onStop();
		GoogleAnalytics.getInstance(this).reportActivityStop(this);
	}
	

}
