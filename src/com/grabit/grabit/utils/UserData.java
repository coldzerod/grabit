package com.grabit.grabit.utils;

import java.lang.reflect.Type;
import java.util.ArrayList;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class UserData {

	private static UserData INSTANCE;

	private static final String PREFS_NAME = "GRAPIT_USERDATA";

	/*
	 * These keys will tell the shared preferences editor which data we're
	 * trying to access
	 */

	private static final String SEARCHD_USERS_HIS = "searched_users_his";

	private static final String EDITED[] = { "ed1", "ed2", "ed3", "ed4", "ed5" };
	/*
	 * Create our shared preferences object and editor which will be used to
	 * save and load data
	 */

	private SharedPreferences mSettings;
	private SharedPreferences.Editor mEditor;

	public boolean mEditedLevel[] = { false, false, false, false, false };

	private String msearched_users_His;

	private ArrayList<String> msearched_users_his = new ArrayList<String>();

	UserData() {

	}

	public synchronized static UserData getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new UserData();
			// Log.v("", "Creates a new instance of UserData");
		}
		// Log.v("", "returns the instance of UserData");
		return INSTANCE;
	}

	public synchronized void init(Context pContext) {
		if (mSettings == null) {

			// use static Context.MODE_PRIVATE, instead pContext.MODE_PRIVATE
			// as an argument in getSharedPreferences
			mSettings = pContext.getSharedPreferences(PREFS_NAME,
					Context.MODE_PRIVATE);

			mEditor = mSettings.edit();

			// same key used for level, highscore, and editedlevel
			// which means all these variables will have the same value
			// determined by "GAME_USERDATA" key

			for (int i = 0; i < 5; i++)
				mEditedLevel[i] = mSettings.getBoolean(EDITED[i], false);

			// msearched_users_His=mSettings.getString(SEARCHD_USERS_HIS, "");
			// msearched_users_His_Count=mSettings.getString(SEARCHD_USERS_HIS_COUNT,
			// "");

			Gson gson = new Gson();
			String json = gson.toJson(msearched_users_his);
			msearched_users_His = mSettings.getString(SEARCHD_USERS_HIS, json);
		}
	}

	public synchronized boolean getEdited(int i) {
		// Log.v("", "mEditedLevel before increment " + mEditedLevel[i]);
		return mEditedLevel[i];
	}

	public synchronized void addUserHistory(String User_Name) {
		Gson gson = new Gson();
		String json = mSettings.getString(SEARCHD_USERS_HIS,
				msearched_users_His);
		Type type = new TypeToken<ArrayList<String>>() {
		}.getType();
		msearched_users_his = gson.fromJson(json, type);

		int index = msearched_users_his.indexOf(User_Name);
		if (index == -1)
			msearched_users_his.add(User_Name);

		json = gson.toJson(msearched_users_his);
		mEditor.putString(SEARCHD_USERS_HIS, json);

		mEditor.commit();
	}

	public synchronized ArrayList<String> getUserHistory() {
		Gson gson = new Gson();

		String json = mSettings.getString(SEARCHD_USERS_HIS,
				msearched_users_His);

		Type type = new TypeToken<ArrayList<String>>() {
		}.getType();

		msearched_users_his = gson.fromJson(json, type);

		return msearched_users_his;
	}

	public synchronized void setEdited(int i, final boolean newEdited) {
		mEditedLevel[i] = newEdited;
		// Log.v("", "mEditedLevel is " + mEditedLevel[i]);
		mEditor.putBoolean(EDITED[i], mEditedLevel[i]);
		mEditor.commit();
	}
}
