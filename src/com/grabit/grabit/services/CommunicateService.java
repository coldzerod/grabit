package com.grabit.grabit.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Binder;
import android.os.IBinder;

import com.grabit.grabit.ui.activities.AboutActivity;

public class CommunicateService extends Service {

	private final IBinder mBinder = new IMBinder();
	public Boolean isDataLoaded = false;
	private boolean authenticatedUser = false;

	public ArrayList<Bitmap> downloaded_profile_pictures = new ArrayList<Bitmap>();
	public ArrayList<Bitmap> downloaded_cover_pictures = new ArrayList<Bitmap>();
	public ArrayList<String> downloaded_profiles_names = new ArrayList<String>();

	public class IMBinder extends Binder {
		public CommunicateService getService() {
			return CommunicateService.this;
		}

	}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return mBinder;
	}

	@Override
	public void onDestroy() {
		// Log.i("Communication service is being destroyed", "...");
		super.onDestroy();
	}

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		// Log.v("Service", "Created");
	}

	public boolean isUserAuthenticated() {
		return authenticatedUser;
	}

	private String processURL(String URL) {
		try {

			String url = "http://graph.facebook.com/" + URL
					+ "/picture?width=2000&height=2000";

			URL obj = new URL(url);
			HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
			conn.setReadTimeout(5000);
			conn.addRequestProperty("Accept-Language", "en-US,en;q=0.8");
			conn.addRequestProperty("User-Agent", "Mozilla");
			conn.addRequestProperty("Referer", "google.com");

			// System.out.println("Request URL ... " + url);

			boolean redirect = false;

			// normally, 3xx is redirect
			int status = conn.getResponseCode();
			if (status != HttpURLConnection.HTTP_OK) {
				if (status == HttpURLConnection.HTTP_MOVED_TEMP
						|| status == HttpURLConnection.HTTP_MOVED_PERM
						|| status == HttpURLConnection.HTTP_SEE_OTHER)
					redirect = true;
			}

			// System.out.println("Response Code ... " + status);

			if (redirect) {

				// get redirect url from "location" header field
				String newUrl = conn.getHeaderField("Location");

				// get the cookie if need, for login
				String cookies = conn.getHeaderField("Set-Cookie");

				// open the new connnection again
				conn = (HttpURLConnection) new URL(newUrl).openConnection();
				conn.setRequestProperty("Cookie", cookies);
				conn.addRequestProperty("Accept-Language", "en-US,en;q=0.8");
				conn.addRequestProperty("User-Agent", "Mozilla");
				conn.addRequestProperty("Referer", "google.com");

				// saveImage(newUrl);

				return newUrl;

			} else {
				return "EMPTY";
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "EMPTY";
	}

	private Bitmap downloadUrl(String strUrl) throws IOException {
		Bitmap bitmap = null;
		InputStream iStream = null;
		try {
			URL url = new URL(strUrl);
			/** Creating an http connection to communcate with url */
			HttpURLConnection urlConnection = (HttpURLConnection) url
					.openConnection();

			/** Connecting to url */
			urlConnection.connect();

			/** Reading data from url */
			iStream = urlConnection.getInputStream();

			/** Creating a bitmap from the stream returned from the url */
			bitmap = BitmapFactory.decodeStream(iStream);

		} catch (Exception e) {
			// Log.v("Exception while downloading url", e.toString());
		} finally {
			if (iStream != null)
				iStream.close();
		}
		return bitmap;
	}

	public void LoadProfilesPictures(String providedProfiles) {

		downloaded_profile_pictures.clear();
		downloaded_profiles_names.clear();
		String res;

		Bitmap temp;
		String[] profiles_requested = providedProfiles.trim().split("(\\,)");

		for (int i = 0; i < profiles_requested.length; i++) {
			profiles_requested[i]=profiles_requested[i].replaceAll("\\s+","");
			res = processURL(profiles_requested[i].trim());
			if (res.indexOf("EMPTY") == -1) {

				try {
					temp = downloadUrl(res);

					if (temp != null) {
						downloaded_profile_pictures.add(temp);
						downloaded_profiles_names.add(profiles_requested[i]
								.trim());
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else
				downloaded_profiles_names.add(null);
		}

		isDataLoaded = true;
	}

	public void LoadProfilesCover(String providedProfiles) throws JSONException {

		downloaded_cover_pictures.clear();
		String res;

		Bitmap temp;
		String[] profiles_requested = providedProfiles.trim().split("(\\,)");

		for (int i = 0; i < profiles_requested.length; i++) {
			profiles_requested[i]=profiles_requested[i].replaceAll("\\s+","");
			res = readCoverInfo(profiles_requested[i].trim());
			if (res.indexOf("EMPTY") == -1) {

				res = processJson(res);

				if (res.indexOf("EMPTY") == -1) {// after Json

					try {
						temp = downloadUrl(res);

						if (temp != null) {
							downloaded_cover_pictures.add(temp);
						}

					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

		}
	}

	public String processJson(String json) {

		String url = "EMPTY";
		JSONObject JODetails;

		try {

			JODetails = new JSONObject(json);

			if (JODetails.has("cover")) {

				String getInitialCover = JODetails.getString("cover");

				if (getInitialCover.equals("null")) {
					return "EMPTY";
				} else {
					JSONObject JOCover = JODetails.optJSONObject("cover");

					if (JOCover.has("source")) {
						url = JOCover.getString("source");
					} else {
						return "EMPTY";
					}
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return url;

	}

	public String readCoverInfo(String user_name) {
		StringBuilder builder = new StringBuilder();
		HttpClient client = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet("https://graph.facebook.com/" + user_name
				+ "?fields=cover");
		try {
			HttpResponse response = client.execute(httpGet);
			StatusLine statusLine = response.getStatusLine();
			int statusCode = statusLine.getStatusCode();
			if (statusCode == 200) {
				HttpEntity entity = response.getEntity();
				InputStream content = entity.getContent();
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(content));
				String line;
				while ((line = reader.readLine()) != null) {
					builder.append(line);
				}
			} else {
				return "EMPTY";
				// Log.e(ParseJSON.class.toString(), "Failed to download file");
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return builder.toString();
	}

	public void About() {
		Intent intent = new Intent(getBaseContext(), AboutActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.putExtra("type", "about");
		startActivity(intent);
	}

}
